void setup (){
  strokeWeight(0);
  size(1200,900);
  fill(76, 23,223); // sky blue
  rect(0,0,1200,500);
   arc(120,650,210,200, PI, TWO_PI);
  arc(360,650,210,200, PI, TWO_PI);
  arc(600,650,210,200, PI, TWO_PI);
  arc(840,650,210,200, PI, TWO_PI);
  arc(1080,650,210,200, PI, TWO_PI);
  fill(6, 183, 24);// grass green
  rect(0,700,1200,900);
  rect(15,650,210,50);
  rect(255,650,210,50);
  rect(495,650,210,50);
  rect(735,650,210,50);
  rect(975,650,210,50);
  strokeWeight(3);
  line(0,500,1200,500);
  line(0,700,1200,700);
  
}

void draw(){
 
}
